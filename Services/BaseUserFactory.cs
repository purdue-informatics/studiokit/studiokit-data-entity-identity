using Microsoft.AspNetCore.Identity;
using StudioKit.Data.Entity.Identity.Interfaces;
using System;

namespace StudioKit.Data.Entity.Identity.Services;

public class BaseUserFactory<TUser> : IUserFactory<TUser>
	where TUser : IUser, new()
{
	private readonly ILookupNormalizer _lookupNormalizer;

	public BaseUserFactory(ILookupNormalizer lookupNormalizer)
	{
		_lookupNormalizer = lookupNormalizer ?? throw new ArgumentNullException(nameof(lookupNormalizer));
	}

	public TUser CreateUser(
		string userName,
		string email,
		string firstName = null,
		string lastName = null,
		string employeeNumber = null,
		string uid = null,
		string id = null)
	{
		var user = new TUser
		{
			UserName = userName,
			NormalizedUserName = _lookupNormalizer.NormalizeName(userName),
			Email = email,
			NormalizedEmail = _lookupNormalizer.NormalizeEmail(email),
			FirstName = firstName,
			LastName = lastName,
			EmployeeNumber = employeeNumber,
			Uid = uid
		};

		// Id is initialized with a GUID by IdentityUser. Only overwrite that if we have been passed in an Id.
		if (id != null)
			user.Id = id;

		return user;
	}
}