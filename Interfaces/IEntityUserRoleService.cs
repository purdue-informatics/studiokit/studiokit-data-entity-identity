﻿using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Data.Entity.Identity.Interfaces;

/// <summary>
/// Service interface for the system to perform bulk actions on <see cref="IEntityUserRole"/>s.
/// </summary>
/// <typeparam name="TEntityUserRole">The type of EntityUserRole</typeparam>
public interface IEntityUserRoleService<TEntityUserRole>
	where TEntityUserRole : IEntityUserRole
{
	Task AddRangeAsync(List<TEntityUserRole> entityUserRoles, IPrincipal principal, CancellationToken cancellationToken = default);

	Task DeleteRangeAsync(List<TEntityUserRole> entityUserRoles, IPrincipal principal, bool throwIfRemovingAllOwners = true,
		CancellationToken cancellationToken = default);
}