﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity.Models;

namespace StudioKit.Data.Entity.Identity.Interfaces;

/// <summary>
/// Interface for an <see cref="IdentityDbContext"/> with TUser, TIdentityProvider, and other Identity framework entities
/// </summary>
/// <typeparam name="TUser">Type of <see cref="IUser"/></typeparam>
/// <typeparam name="TIdentityProvider">Type of <see cref="IdentityProvider"/></typeparam>
public interface IUserIdentityDbContext<TUser, TIdentityProvider>
	where TUser : IdentityUser, IUser
	where TIdentityProvider : IdentityProvider, new()
{
	DbSet<TUser> Users { get; set; }

	DbSet<UserRole> IdentityUserRoles { get; set; }

	DbSet<UserLogin> IdentityUserLogins { get; set; }

	DbSet<UserClaim> IdentityUserClaims { get; set; }

	DbSet<TIdentityProvider> IdentityProviders { get; set; }
}