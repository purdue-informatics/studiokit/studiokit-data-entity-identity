﻿namespace StudioKit.Data.Entity.Identity.Interfaces;

public interface IGroupUserRole : IEntityUserRole
{
	int GroupId { get; set; }

	bool IsExternal { get; set; }
}