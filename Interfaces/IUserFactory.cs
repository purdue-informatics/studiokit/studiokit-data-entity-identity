namespace StudioKit.Data.Entity.Identity.Interfaces;

public interface IUserFactory<out TUser>
	where TUser : IUser, new()
{
	TUser CreateUser(
		string userName,
		string email,
		string firstName = null,
		string lastName = null,
		string employeeNumber = null,
		string uid = null,
		string id = null);
}