﻿namespace StudioKit.Data.Entity.Identity.Interfaces;

public interface IGroupUserRoleService<TGroupUserRole> : IEntityUserRoleService<TGroupUserRole>
	where TGroupUserRole : IGroupUserRole
{
}