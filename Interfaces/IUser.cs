﻿using StudioKit.Data.Interfaces;

namespace StudioKit.Data.Entity.Identity.Interfaces;

public interface IUser : IAuditable
{
	/// <summary>Unique key for the user</summary>
	string Id { get; set; }

	/// <summary>Unique username</summary>
	string UserName { get; set; }

	/// <summary>Uppercase Normalized Unique username</summary>
	string NormalizedUserName { get; set; }

	/// <summary>
	/// Equivalent to CareerAccountAlias for Purdue.
	/// </summary>
	string Uid { get; set; }

	string CareerAccountAlias { get; set; }

	/// <summary>
	/// Equivalent to PUID for Purdue.
	/// </summary>
	string EmployeeNumber { get; set; }

	string Puid { get; set; }

	string FirstName { get; set; }

	string LastName { get; set; }

	string FullName { get; }

	string Email { get; set; }

	string NormalizedEmail { get; set; }
}