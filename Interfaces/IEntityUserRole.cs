﻿namespace StudioKit.Data.Entity.Identity.Interfaces;

public interface IEntityUserRole
{
	int Id { get; set; }

	int EntityId { get; set; }

	string UserId { get; set; }

	string RoleId { get; set; }
}