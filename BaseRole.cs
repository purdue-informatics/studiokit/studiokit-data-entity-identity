﻿using System;
using System.Collections.Generic;

namespace StudioKit.Data.Entity.Identity;

public static class BaseRole
{
	public const string SuperAdmin = "SuperAdmin";
	public const string Admin = "Admin";
	public const string Creator = "Creator";
	public const string DatabaseAdmin = "DatabaseAdmin";

	public static List<string> GlobalRoles = new List<string> { SuperAdmin, Admin, Creator, DatabaseAdmin };

	public const string GroupOwner = "GroupOwner";
	public const string GroupLearner = "GroupLearner";
	public const string GroupGrader = "GroupGrader";

	public static List<string> GroupRoles = new List<string> { GroupOwner, GroupLearner, GroupGrader };

	public static string TextForRole(string roleName)
	{
		switch (roleName)
		{
			case Admin:
			case Creator:
			case DatabaseAdmin:
				return roleName;

			case SuperAdmin: return "Super Admin";
			case GroupOwner: return "Instructor";
			case GroupLearner: return "Student";
			case GroupGrader: return "Grader";
			default: return null;
		}
	}

	public static string SingularArticleForRole(string role)
	{
		switch (role)
		{
			case Admin:
			case GroupOwner:
				return "an";

			case SuperAdmin:
			case DatabaseAdmin:
			case Creator:
			case GroupGrader:
			case GroupLearner:
				return "a";

			default:
				throw new ArgumentException($"role '{role}' was not found in switch statement");
		}
	}

	public static string QuantityTextForRole(string role, int quantity)
	{
		return quantity == 1 ? $"{SingularArticleForRole(role)} {TextForRole(role)}" : $"{TextForRole(role)}s";
	}
}