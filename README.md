﻿# StudioKit.Data.Entity.Identity

**Note**: Requires the following peer dependencies in the Solution.
* **StudioKit.Data.Entity**

Extends the EntityFramework models from **StudioKit.Data.Entity**. Using **Microsoft.AspNet.Identity.EntityFramework**, defines an `IdentityDbContext` with custom User and IdentityProvider classes. Provides other models commonly used with Users.

## Base

* **UserIdentityDbContext**
	* An `IUserIdentityDbContext` to be used as a base class for your DbContext.
* Role
	* Common static string constants for use as IdentityRoles.

### Interfaces

* IUser
	* Interface of general User properties. Implement this interface on your User model.
* IUserIdentityDbContext
	* Interface for an IdentityDbContext, with sets of IUser implemented Users, and IdentityProviders.

### Models

* IdentityProvider
	* Included in `IUserIdentityDbContext`. Represents log in options.
* Login
	* (Optional) Represents a User log in event. Add a DbSet on your DbContext to use this model.
* UserAgreement
	* (Optional) Represents a User Agreement. Add a DbSet on your DbContext to use this model.
* Contract
	* (Optional) Represents the many-to-many between User and UserAgreement. Add a DbSet on your DbContext to use this model.
* UserSetting
	* (Optional) Represents User defined settings. Add a DbSet on your DbContext to use this model. Create additional subclasses for additional settings.
