using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Data.Entity.Utils;
using System.Data.Common;

namespace StudioKit.Data.Entity.Identity;

/// <summary>
/// Subclass of <see cref="IdentityDbContext"/> with TUser, TIdentityProvider, and other Identity framework entities
/// </summary>
/// <typeparam name="TUser">Type of <see cref="IUser"/></typeparam>
/// <typeparam name="TIdentityProvider">Type of <see cref="IdentityProvider"/></typeparam>
public class UserIdentityDbContext<TUser, TIdentityProvider>
	: IdentityDbContext<TUser, Role, string, UserClaim, UserRole, UserLogin, IdentityRoleClaim<string>, IdentityUserToken<string>>,
		IUserIdentityDbContext<TUser, TIdentityProvider>
	where TUser : IdentityUser, IUser
	where TIdentityProvider : IdentityProvider, new()
{
	private readonly DbConnection _dbConnection;
	protected readonly string NameOrConnectionString;

	public UserIdentityDbContext()
	{
	}

	public UserIdentityDbContext(DbContextOptions options) : base(options)
	{
	}

	public UserIdentityDbContext(DbConnection existingConnection)
	{
		_dbConnection = existingConnection;
	}

	public UserIdentityDbContext(string nameOrConnectionString)
	{
		NameOrConnectionString = nameOrConnectionString;
	}

	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	{
		if (_dbConnection != null)
		{
			optionsBuilder.UseSqliteOrSqlServer(_dbConnection);
		}
		else if (!string.IsNullOrWhiteSpace(NameOrConnectionString))
		{
			optionsBuilder.UseSqliteOrSqlServer(NameOrConnectionString);
		}
	}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);

		if (Database.IsSqlite())
		{
			SqliteTimestampConverter.Configure(builder);
		}
	}

	public virtual DbSet<UserRole> IdentityUserRoles { get; set; }

	public virtual DbSet<UserLogin> IdentityUserLogins { get; set; }

	public virtual DbSet<UserClaim> IdentityUserClaims { get; set; }

	public virtual DbSet<TIdentityProvider> IdentityProviders { get; set; }
}