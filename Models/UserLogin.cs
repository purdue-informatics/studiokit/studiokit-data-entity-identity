﻿using Microsoft.AspNetCore.Identity;
using StudioKit.Data.Interfaces;
using System;

namespace StudioKit.Data.Entity.Identity.Models;

public class UserLogin : IdentityUserLogin<string>, IAuditable
{
	public DateTime DateStored { get; set; }

	public DateTime DateLastUpdated { get; set; }

	public string LastUpdatedById { get; set; }
}