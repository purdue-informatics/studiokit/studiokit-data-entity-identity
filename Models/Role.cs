﻿using Microsoft.AspNetCore.Identity;
using StudioKit.Data.Interfaces;
using System;

namespace StudioKit.Data.Entity.Identity.Models;

public class Role : IdentityRole, IAuditable
{
	public Role()
	{
		Id = Guid.NewGuid().ToString();
	}

	public Role(string roleName) : this()
	{
		Name = roleName;
	}

	public DateTime DateStored { get; set; }

	public DateTime DateLastUpdated { get; set; }

	public string LastUpdatedById { get; set; }
}