﻿using Microsoft.AspNetCore.Identity;
using StudioKit.Data.Interfaces;
using System;

namespace StudioKit.Data.Entity.Identity.Models;

public class UserClaim : IdentityUserClaim<string>, IAuditable
{
	public string ClaimValueType { get; set; }

	public string ClaimIssuer { get; set; }

	public string ClaimOriginalIssuer { get; set; }

	public DateTime DateStored { get; set; }

	public DateTime DateLastUpdated { get; set; }

	public string LastUpdatedById { get; set; }
}